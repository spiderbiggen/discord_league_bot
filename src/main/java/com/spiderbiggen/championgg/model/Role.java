package com.spiderbiggen.championgg.model;

import java.util.Arrays;
import java.util.Optional;

public enum Role {
    DUO_SUPPORT("Support"),
    JUNGLE("Jungle"),
    TOP("Top"),
    MIDDLE("Mid"),
    DUO_CARRY("Bot");

    private String name;

    Role(String name) {
        this.name = name;
    }

    public static Role parse(String s) {
        String ls = s.toLowerCase();
        Optional<Role> role = Arrays.stream(Role.values()).filter(r -> r.name.toLowerCase().equals(ls)).findAny();
        return role.orElseGet(() -> Role.valueOf(s.toUpperCase()));
    }

    @Override
    public String toString() {
        return name;
    }
}

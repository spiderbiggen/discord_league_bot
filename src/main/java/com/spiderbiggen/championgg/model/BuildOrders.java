package com.spiderbiggen.championgg.model;


public class BuildOrders {

    private BuildChoice finalitemshashfixed;
    private BuildChoice masterieshash;
    private BuildChoice skillorderhash;
    private BuildChoice summonershash;
    private BuildChoice trinkethash;
    private BuildChoice firstitemshash;
    private BuildChoice runehash;

    public BuildChoice getFinalitemshashfixed() {
        return finalitemshashfixed;
    }

    public void setFinalitemshashfixed(BuildChoice finalitemshashfixed) {
        this.finalitemshashfixed = finalitemshashfixed;
    }

    public BuildChoice getMasterieshash() {
        return masterieshash;
    }

    public void setMasterieshash(BuildChoice masterieshash) {
        this.masterieshash = masterieshash;
    }

    public BuildChoice getSkillorderhash() {
        return skillorderhash;
    }

    public void setSkillorderhash(BuildChoice skillorderhash) {
        this.skillorderhash = skillorderhash;
    }

    public BuildChoice getSummonershash() {
        return summonershash;
    }

    public void setSummonershash(BuildChoice summonershash) {
        this.summonershash = summonershash;
    }

    public BuildChoice getTrinkethash() {
        return trinkethash;
    }

    public void setTrinkethash(BuildChoice trinkethash) {
        this.trinkethash = trinkethash;
    }

    public BuildChoice getFirstitemshash() {
        return firstitemshash;
    }

    public void setFirstitemshash(BuildChoice firstitemshash) {
        this.firstitemshash = firstitemshash;
    }

    public BuildChoice getRunehash() {
        return runehash;
    }

    public void setRunehash(BuildChoice runehash) {
        this.runehash = runehash;
    }

}

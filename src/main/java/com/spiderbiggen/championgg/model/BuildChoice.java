package com.spiderbiggen.championgg.model;

import java.util.Arrays;

public class BuildChoice {

    private Hash highestCount;
    private Hash highestWinrate;

    public Hash getHighestCount() {
        return highestCount;
    }

    public void setHighestCount(Hash highestCount) {
        this.highestCount = highestCount;
    }

    public Hash getHighestWinrate() {
        return highestWinrate;
    }

    public void setHighestWinrate(Hash highestWinrate) {
        this.highestWinrate = highestWinrate;
    }

    public static class Hash {
        private int count;
        private int wins;
        private float winrate;
        private String hash;

        public Hash() {
        }

        public Hash(String hash) {
            this.hash = hash;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public int getWins() {
            return wins;
        }

        public void setWins(int wins) {
            this.wins = wins;
        }

        public float getWinrate() {
            return winrate;
        }

        public void setWinrate(float winrate) {
            this.winrate = winrate;
        }

        public String getHash() {
            return hash;
        }

        public void setHash(String hash) {
            this.hash = hash;
        }

        public int[] getHashAsInts() {
            String[] hashes = hash.replaceAll("[^0-9-]", "").split("-");
            return Arrays.stream(hashes).filter(s -> !s.isEmpty()).mapToInt(Integer::parseInt).toArray();
        }

        public char[] getAsAbilityOrder() {
            String hashes = hash.replaceAll("[^QWER]", "");
            return hashes.toCharArray();
        }

    }
}

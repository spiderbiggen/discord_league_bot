package com.spiderbiggen.championgg.model;


public class Build {

    private String patch;
    private int championId;
    private float winRate;
    private float playRate;
    private int gamesPlayed;
    private float percentRolePlayed;
    private float banRate;
    private Role role;
    private BuildOrders hashes;

    public String getPatch() {
        return patch;
    }

    public void setPatch(String patch) {
        this.patch = patch;
    }

    public int getChampionId() {
        return championId;
    }

    public void setChampionId(int championId) {
        this.championId = championId;
    }

    public float getWinRate() {
        return winRate;
    }

    public void setWinRate(float winRate) {
        this.winRate = winRate;
    }

    public float getPlayRate() {
        return playRate;
    }

    public void setPlayRate(float playRate) {
        this.playRate = playRate;
    }

    public int getGamesPlayed() {
        return gamesPlayed;
    }

    public void setGamesPlayed(int gamesPlayed) {
        this.gamesPlayed = gamesPlayed;
    }

    public float getPercentRolePlayed() {
        return percentRolePlayed;
    }

    public void setPercentRolePlayed(float percentRolePlayed) {
        this.percentRolePlayed = percentRolePlayed;
    }

    public float getBanRate() {
        return banRate;
    }

    public void setBanRate(float banRate) {
        this.banRate = banRate;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public BuildOrders getHashes() {
        return hashes;
    }

    public void setHashes(BuildOrders hashes) {
        this.hashes = hashes;
    }

    @Override
    public String toString() {
        return "Build{" +
                "patch='" + patch + '\'' +
                ", championId=" + championId +
                ", role=" + role.name() +
                '}';
    }
}

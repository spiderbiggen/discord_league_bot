package com.spiderbiggen.championgg;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.spiderbiggen.championgg.model.Build;
import com.spiderbiggen.leaguestats.util.HttpRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChampionGG {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChampionGG.class);
    private final String apiKey;

    public ChampionGG(String apiKey) {
        this.apiKey = apiKey;
    }

    public List<Build> getRolesAndBuilds() {
        String query = String.format("http://api.champion.gg/v2/champions?limit=500&api_key=%s&champData=hashes", apiKey);
        Gson gson = new GsonBuilder().create();
        try (HttpRequest request = new HttpRequest(query);
             InputStream response = request.connect().getResponseStream();
             InputStreamReader reader = new InputStreamReader(response)) {
            Build[] arr = gson.fromJson(reader, Build[].class);
            LOGGER.debug("Retrieved champions from champion gg api.");
            return Arrays.asList(arr);
        } catch (IOException e) {
            LOGGER.debug("An unexpected error occurred retreiving champions from champion gg api.", e);
        }
        return new ArrayList<>();
    }
}

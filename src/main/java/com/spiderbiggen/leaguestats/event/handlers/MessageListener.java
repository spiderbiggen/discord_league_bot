package com.spiderbiggen.leaguestats.event.handlers;

import com.mingweisamuel.zyra.RiotApi;
import com.spiderbiggen.leaguestats.registries.CommandRegistry;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.*;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created on 17-6-2017.
 *
 * @author Stefan Breetveld
 */
public class MessageListener extends ListenerAdapter {

    public static final String PREFIX = "lol";
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageListener.class);

    private User self;
    private RiotApi api;

    public MessageListener(RiotApi api) {
        this.api = api;
    }

    @Override
    public void onReady(ReadyEvent event) {
        self = event.getJDA().getSelfUser();
        LOGGER.info("Logged in successfully");
    }

    @Override
    public void onShutdown(ShutdownEvent event) {
        LOGGER.info("Shutdown successfully");
    }

    @Override
    public void onDisconnect(DisconnectEvent event) {
        LOGGER.info("Disconnected");
    }

    @Override
    public void onResume(ResumedEvent event) {
        LOGGER.info("Resumed Operation");
    }

    @Override
    public void onReconnect(ReconnectedEvent event) {
        LOGGER.info("Reconnected");
    }

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        MessageChannel channel = event.getChannel();
        User user = event.getAuthor();
        Message message = event.getMessage();
        String content = message.getContentRaw();
        if (content.startsWith(PREFIX) || message.isMentioned(self, Message.MentionType.USER)) {
            handleMessage(message, user, channel);
        }
    }

    @Override
    public void onPrivateMessageReceived(PrivateMessageReceivedEvent event) {
        MessageChannel channel = event.getChannel();
        User user = event.getAuthor();
        Message message = event.getMessage();
        handleMessage(message, user, channel);
    }

    private void handleMessage(Message message, User user, MessageChannel channel) {
        if (user.equals(self)) return;
        String content = message.getContentRaw();
        if (content.startsWith(PREFIX)) {
            content = content.substring(PREFIX.length());
        }
        content = content.replaceAll(self.getAsMention(), "").trim();
        LOGGER.debug(content);
        CommandRegistry.execute(content, user, channel, api);
    }
}

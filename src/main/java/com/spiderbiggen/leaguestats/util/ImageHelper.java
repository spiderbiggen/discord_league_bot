package com.spiderbiggen.leaguestats.util;


import com.spiderbiggen.championgg.model.Build;
import com.spiderbiggen.championgg.model.BuildOrders;
import com.spiderbiggen.leaguestats.database.RiotDb;
import com.spiderbiggen.leaguestats.model.SummonerSpell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;

import static java.awt.image.BufferedImage.TYPE_INT_ARGB;

public class ImageHelper {

    private static final int ICON_SIZE = 64;
    private static final Logger LOGGER = LoggerFactory.getLogger(ImageHelper.class);
    private static final int MAX_FIRST_BUY = 5;
    private static final int MAX_FINAL_BUY = 6;
    private static final int MAX_SUMMONERS = 2;
    private static final int MAX_LEVEL = 18;

    public static byte[] createBuildImageBytes(Build build) throws IOException {
        return toByteArray(createBuildImage(build));
    }

    public static byte[] toByteArray(BufferedImage image) {
        byte[] imageInByte = new byte[0];
        if (image != null) {
            try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
                ImageIO.write(image, "png", outputStream);
                outputStream.flush();
                imageInByte = outputStream.toByteArray();
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
        return imageInByte;
    }

    public static BufferedImage createBuildImage(Build build) throws IOException {
        String version = null;
        try (RiotDb connection = RiotDb.create()) {
            version = connection.getVersion("item");
        } catch (SQLException e) {
            LoggerUtils.logGenericDatabaseError(LOGGER, e);
        }
        if (version == null)
            return null;

        File imageDir = new File("images", version);
        File itemDir = new File(imageDir, "item");
        File spellDir = new File(imageDir, "spell");

        if ((itemDir.exists() || itemDir.mkdirs()) && (spellDir.exists() || spellDir.mkdirs())) {
            BuildOrders buildOrders = build.getHashes();
            int[] firstItems = buildOrders.getFirstitemshash().getHighestCount().getHashAsInts();
            int[] finalItems = buildOrders.getFinalitemshashfixed().getHighestCount().getHashAsInts();
            int[] summoners = buildOrders.getSummonershash().getHighestCount().getHashAsInts();
            char[] spellOrder = buildOrders.getSkillorderhash().getHighestCount().getAsAbilityOrder();
            BuildImageData data = new BuildImageData(firstItems, finalItems, summoners, spellOrder);
            BuildImageFiles files = downloadImages(data, itemDir, spellDir, version);
            if (files == null)
                return null;
            return compileImage(files);
        }
        return null;
    }

    private static BufferedImage compileImage(BuildImageFiles files) throws IOException {
        //creating a bufferd image array from image files
        BufferedImage overlay = ImageIO.read(ImageHelper.class.getResourceAsStream("/build.png"));
        BufferedImage[] firstBuy = bufferImages(files.firstBuy);
        BufferedImage[] finalBuy = bufferImages(files.finalBuy);
        BufferedImage[] summoners = bufferImages(files.summoners);

        //Initializing the final image
        BufferedImage finalImg = new BufferedImage(608, 288, TYPE_INT_ARGB);

        drawImageArrayHorizontal(20, 20, 20, finalImg, firstBuy);
        drawImageArrayHorizontal(440, 20, 20, finalImg, summoners);
        drawImageArrayHorizontal(20, 124, 20, finalImg, finalBuy);
        finalImg.createGraphics().drawImage(overlay, 0, 0, null);
        drawCharArrayHorizontal(20, 248, finalImg, files.spellOrder);
        return finalImg;
    }

    private static BuildImageFiles downloadImages(BuildImageData data, File itemDir, File spellDir, String version) {
        BuildImageFiles files = new BuildImageFiles(data.spellOrder);
        int[] finalBuy = data.finalBuy;
        int[] firstBuy = data.firstBuy;
        for (int i = 0, firstBuyLength = firstBuy.length; i < firstBuyLength; i++) {
            int id = firstBuy[i];
            if (id <= 0) continue;
            File file = new File(itemDir, id + ".png");
            files.firstBuy[i] = downloadItemIcon(id, file, version);
        }
        for (int i = 0, finalBuyLength = finalBuy.length; i < finalBuyLength; i++) {
            int id = finalBuy[i];
            File file = new File(itemDir, id + ".png");
            files.finalBuy[i] = downloadItemIcon(id, file, version);
        }
        int[] summoners = data.summoners;
        for (int i = 0, summonersLength = summoners.length; i < summonersLength; i++) {
            int id = summoners[i];
            File file = new File(spellDir, id + ".png");
            files.summoners[i] = downloadSummonerIcon(id, file, version);
        }
        return files;
    }

    private static File downloadItemIcon(int id, File file, String version) {
        String url = "http://ddragon.leagueoflegends.com/cdn/" + version + "/img/item/" + id + ".png";
        return downloadImage(url, file);
    }

    private static File downloadSummonerIcon(int id, File file, String version) {
        SummonerSpell spell = SummonerSpell.getFromKey(id);
        String url = "http://ddragon.leagueoflegends.com/cdn/" + version + "/img/spell/" + spell.getId() + ".png";
        return downloadImage(url, file);
    }

    private static File downloadImage(String url, File file) {
        if (!file.exists()) {
            boolean success = false;
            try {
                success = file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (success) {
                try (HttpRequest request = new HttpRequest(url);
                     InputStream inputStream = request.getResponseStream();
                     OutputStream outputStream = new FileOutputStream(file)) {
                    byte[] buffer = new byte[8 * 1024];
                    int bytesRead;
                    while ((bytesRead = inputStream.read(buffer)) != -1) {
                        outputStream.write(buffer, 0, bytesRead);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return file;
    }

    private static BufferedImage[] bufferImages(File[] files) throws IOException {
        int listSize = files.length;
        BufferedImage[] images = new BufferedImage[listSize];
        for (int i = 0; i < listSize; i++) {
            File file = files[i];
            if (file != null) {
                images[i] = ImageIO.read(file);
            }
        }
        return images;
    }

    private static void drawImageArrayHorizontal(int initialXOffset, int initialYOffset, int xSpacing, BufferedImage finalImage, BufferedImage... images) {
        int x = initialXOffset;
        for (BufferedImage image : images) {
            if (image != null)
                finalImage.createGraphics().drawImage(image, x, initialYOffset, ICON_SIZE, ICON_SIZE, null);
            x += xSpacing + ICON_SIZE;
        }
    }

    private static void drawCharArrayHorizontal(int initialXOffset, int initialYOffset, BufferedImage finalImage, char... args) {
        float x = initialXOffset;
        Font font = new Font("SansSerif", Font.BOLD, 12);
        try (InputStream stream = ImageHelper.class.getResourceAsStream("/RobotoMono-Bold.ttf")) {
            font = Font.createFont(Font.TRUETYPE_FONT, stream);
        } catch (FontFormatException | IOException e) {
            e.printStackTrace();
        }
        font = font.deriveFont(Font.BOLD, 35f);
        Graphics2D graphics2D = finalImage.createGraphics();
        graphics2D.setFont(font);
        graphics2D.setRenderingHint(
                RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        int charWidth = graphics2D.getFontMetrics().charWidth('Q');
        float spacing = args.length == 1 ? 0 : (568 - charWidth * args.length) / ((float) args.length - 1f) + charWidth;
        for (char character : args) {
            graphics2D.drawString(String.valueOf(character), x, initialYOffset);
            x += spacing;
        }
    }

    private static class BuildImageData {
        private final int[] firstBuy = new int[MAX_FIRST_BUY];
        private final int[] finalBuy = new int[MAX_FINAL_BUY];
        private final int[] summoners = new int[MAX_SUMMONERS];
        private final char[] spellOrder = new char[MAX_LEVEL];

        public BuildImageData(int[] firstBuy, int[] finalBuy, int[] summoners, char[] spellOrder) {
            System.arraycopy(firstBuy, 0, this.firstBuy, 0, firstBuy.length);
            System.arraycopy(finalBuy, 0, this.finalBuy, 0, finalBuy.length);
            System.arraycopy(summoners, 0, this.summoners, 0, summoners.length);
            System.arraycopy(spellOrder, 0, this.spellOrder, 0, spellOrder.length);
        }
    }

    private static class BuildImageFiles {
        private final File[] firstBuy = new File[MAX_FIRST_BUY];
        private final File[] finalBuy = new File[MAX_FINAL_BUY];
        private final File[] summoners = new File[MAX_SUMMONERS];
        private final char[] spellOrder = new char[MAX_LEVEL];

        public BuildImageFiles(char[] spellOrder) {
            System.arraycopy(spellOrder, 0, this.spellOrder, 0, spellOrder.length);
        }
    }
}

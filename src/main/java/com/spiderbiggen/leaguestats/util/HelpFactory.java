package com.spiderbiggen.leaguestats.util;

import static com.spiderbiggen.leaguestats.event.handlers.MessageListener.PREFIX;

public class HelpFactory {
    private static final String START_COMMAND = "***Usage:*** `";
    private static final String END_COMMAND = "`";

    private StringBuilder stringBuilder;

    public HelpFactory() {
        this.stringBuilder = new StringBuilder();
    }

    public HelpFactory startCommand(String name) {
        stringBuilder.append(START_COMMAND).append(PREFIX).append(name);
        return this;
    }

    public HelpFactory endCommand() {
        stringBuilder.append(END_COMMAND);
        return this;
    }

    public HelpFactory addRequiredParameter(String param) {
        stringBuilder.append(" <").append(param).append(">");
        return this;
    }

    public HelpFactory addOptionalParameter(String param) {
        stringBuilder.append(" [").append(param).append("]");
        return this;
    }

    public String build() {
        return stringBuilder.toString();
    }
}

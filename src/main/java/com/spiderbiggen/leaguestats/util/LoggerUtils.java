package com.spiderbiggen.leaguestats.util;

import org.slf4j.Logger;

import java.io.IOException;
import java.sql.SQLException;

public class LoggerUtils {
    private LoggerUtils() {
        // No instances
    }

    public static void logGenericDatabaseError(Logger logger, SQLException e) {
        logger.error("An unexpected error occurred while trying to access the database.", e);
    }

    public static void logGenericRiotApiClosingError(Logger logger, IOException e) {
        logger.error("An unexpected error occurred while trying to close the connection with the riot api.", e);
    }
}

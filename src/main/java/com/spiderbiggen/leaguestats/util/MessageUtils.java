package com.spiderbiggen.leaguestats.util;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.requests.restaction.MessageAction;

import java.awt.*;
import java.io.File;

public class MessageUtils {

    private static final Color LEAGUE_COLOR = new Color(0xC9AA71);

    public static String wrapInCodeBlock(String text) {
        return wrapInCodeBlock(text, "");
    }

    public static String wrapInCodeBlock(String text, String codeLanguage) {
        return String.format("```%s%n%s%n```", codeLanguage, text);
    }

    public static void sendMessage(MessageChannel channel, String s) {
        channel.sendMessage(s).queue();
    }

    public static void sendMessage(MessageChannel channel, MessageEmbed embed) {
        channel.sendMessage(embed).queue();
    }

    public static EmbedBuilder createEmbedBuilder() {
        return new EmbedBuilder().setColor(LEAGUE_COLOR);
    }

    public static void sendFile(MessageChannel channel, String fileName, File file) {
        channel.sendFile(file, fileName).queue();
    }

    public static void sendFile(MessageChannel channel, String fileName, byte[] data) {
        channel.sendFile(data, fileName).queue();
    }
}

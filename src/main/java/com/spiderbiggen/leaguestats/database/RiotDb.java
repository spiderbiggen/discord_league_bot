package com.spiderbiggen.leaguestats.database;

import com.mingweisamuel.zyra.lolStaticData.*;
import com.spiderbiggen.championgg.model.Role;

import java.sql.*;
import java.time.LocalDateTime;

public class RiotDb extends MysqlConnection {

    protected RiotDb() throws SQLException {
        super();
    }

    public static RiotDb create() throws SQLException {
        return new RiotDb();
    }

    @Override
    public void initDatabase() throws SQLException {
        createChampionTable();
        createSkinsTable();
        createVersionTable();
    }

    private void createVersionTable() throws SQLException {
        //language=MySQL
        final String sql = "CREATE TABLE IF NOT EXISTS versions (" +
                "dataType VARCHAR(20) PRIMARY KEY," +
                "version TINYTEXT NOT NULL," +
                "last_update TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" +
                ");";
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        }
    }

    private void createSkinsTable() throws SQLException {
        //language=MySQL
        final String sql = "CREATE TABLE IF NOT EXISTS skins (" +
                "id INT PRIMARY KEY," +
                "num TINYINT," +
                "champion_id INT NOT NULL," +
                "name TINYTEXT" +
                ");";
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        }
    }

    private void createChampionTable() throws SQLException {
        //language=MySQL
        final String sql = "CREATE TABLE IF NOT EXISTS champions( " +
                "id INT PRIMARY KEY, " +
                "`key` TINYTEXT NOT NULL, " +
                "name TINYTEXT NOT NULL, " +
                "title TEXT NOT NULL, " +
                "blurb MEDIUMTEXT, " +
                "partype VARCHAR(20), " +
                //image
                "full TINYTEXT, " +
                "`group` TINYTEXT, " +
                "`sprite` TINYTEXT, " +
                "`h` SMALLINT, " +
                "`w` SMALLINT, " +
                "`x` SMALLINT, " +
                "`y` SMALLINT, " +
                //info
                "`attack` TINYINT, " +
                "`defense` TINYINT, " +
                "`difficulty` TINYINT, " +
                "`magic` TINYINT" +
                ");";
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        }
    }

    public void insertSkinSuppressed(Skin skin, int championId) {
        try {
            insertSkin(skin, championId);
        } catch (SQLException ignored) {
        }
    }

    public void insertSkin(Skin skin, int championId) throws SQLException {
        //language=MySQL
        final String sql = "INSERT INTO skins (`id`, `num`, `champion_id`, `name`) VALUES (?, ?, ?, ?)";
        //language=MySQL
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, skin.id);
            ps.setInt(2, skin.num);
            ps.setInt(3, championId);
            ps.setString(4, skin.name);
            ps.executeUpdate();
        }
    }

    public Skin getRandomSkin(int championId) throws SQLException {
        //language=MySQL
        final String sql = "SELECT * FROM skins WHERE skins.champion_id = ? ORDER BY RAND() LIMIT 1";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, championId);
            try (ResultSet resultSet = ps.executeQuery()) {
                return getSkinFromResultSet(resultSet);
            }
        }
    }

    private Skin getSkinFromResultSet(ResultSet resultSet) throws SQLException {
        if (!resultSet.next()) return null;
        int id = resultSet.getInt("id");
        String name = resultSet.getString("name");
        int num = resultSet.getInt("num");
        return new Skin(id, name, num);
    }

    public void insertChampionsMulti(ChampionList elements) throws SQLException {
        //language=MySQL
        final String insertSql = "REPLACE INTO champions(id, `key`, name, title, blurb, partype, full, `group`, `sprite`, h, w, x, y, attack, defense, difficulty, magic) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,? ,? ,? ,?, ?, ?);";

        boolean oldAutoCommit = connection.getAutoCommit();
        connection.setAutoCommit(false);
        for (Champion champion : elements.data.values()) {
            if (champion.name == null || champion.title == null) continue;
            try (PreparedStatement ps = connection.prepareStatement(insertSql)) {
                ps.setInt(1, champion.id);
                ps.setString(2, champion.key);
                ps.setString(3, champion.name);
                ps.setString(4, champion.title);
                ps.setString(5, champion.blurb);
                ps.setString(6, champion.partype);
                //image
                if (champion.image != null) {
                    ps.setString(7, champion.image.full);
                    ps.setString(8, champion.image.group);
                    ps.setString(9, champion.image.sprite);
                    ps.setInt(10, champion.image.h);
                    ps.setInt(11, champion.image.w);
                    ps.setInt(12, champion.image.x);
                    ps.setInt(13, champion.image.y);
                } else {
                    ps.setNull(7, Types.VARCHAR);
                    ps.setNull(8, Types.VARCHAR);
                    ps.setNull(9, Types.VARCHAR);
                    ps.setNull(10, Types.SMALLINT);
                    ps.setNull(11, Types.SMALLINT);
                    ps.setNull(12, Types.SMALLINT);
                    ps.setNull(13, Types.SMALLINT);
                }
                //info
                if (champion.info != null) {
                    ps.setInt(14, champion.info.attack);
                    ps.setInt(15, champion.info.defense);
                    ps.setInt(16, champion.info.difficulty);
                    ps.setInt(17, champion.info.magic);
                } else {
                    ps.setNull(14, Types.TINYINT);
                    ps.setNull(15, Types.TINYINT);
                    ps.setNull(16, Types.TINYINT);
                    ps.setNull(17, Types.TINYINT);
                }
                ps.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            int championId = champion.id;
            champion.skins.forEach(skin -> insertSkinSuppressed(skin, championId));
        }

        connection.commit();
        connection.setAutoCommit(oldAutoCommit);
    }

    public Champion getRandomChampion() throws SQLException {
        //language=MySQL
        final String sql = "SELECT * FROM champions ORDER BY RAND() LIMIT 1";
        try (PreparedStatement ps = connection.prepareStatement(sql);
             ResultSet resultSet = ps.executeQuery()) {
            return getChampionFromResultSet(resultSet);
        }
    }

    public Champion getRandomChampion(Role role) throws SQLException {
        if (role == null) return getRandomChampion();
        //language=MySQL
        final String sql = "SELECT * FROM champions JOIN builds ON champions.id = builds.id WHERE builds.role = ? ORDER BY RAND() LIMIT 1";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, role.name());
            try (ResultSet resultSet = ps.executeQuery()) {
                return getChampionFromResultSet(resultSet);
            }
        }
    }

    public Champion getChampionByName(String championName) throws SQLException {
        //language=MySQL
        final String sql = "SELECT * FROM champions WHERE `key` LIKE ? OR name LIKE ?;";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, championName);
            ps.setString(2, championName);
            try (ResultSet resultSet = ps.executeQuery()) {
                return getChampionFromResultSet(resultSet);
            }
        }
    }

    public Champion getChampionById(long championId) throws SQLException {
        //language=MySQL
        final String sql = "SELECT * FROM champions WHERE id = ?";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setLong(1, championId);
            try (ResultSet resultSet = ps.executeQuery()) {
                return getChampionFromResultSet(resultSet);
            }
        }
    }

    private Champion getChampionFromResultSet(ResultSet resultSet) throws SQLException {
        resultSet.next();
        int id = resultSet.getInt("id");
        String key = resultSet.getString("key");
        String name = resultSet.getString("name");
        String title = resultSet.getString("title");
        String blurb = resultSet.getString("blurb");
        String partype = resultSet.getString("partype");
        //image
        String imageFull = resultSet.getString("full");
        String imageGroup = resultSet.getString("group");
        String imageSprite = resultSet.getString("sprite");
        int imageH = resultSet.getInt("h");
        int imageW = resultSet.getInt("w");
        int imageX = resultSet.getInt("x");
        int imageY = resultSet.getInt("y");
        Image image = new Image(imageFull, imageGroup, imageH, imageSprite, imageW, imageX, imageY);
        //info
        int infoAttack = resultSet.getInt("attack");
        int infoDefense = resultSet.getInt("defense");
        int infoDifficulty = resultSet.getInt("difficulty");
        int infoMagic = resultSet.getInt("magic");
        Info info = new Info(infoAttack, infoDefense, infoDifficulty, infoMagic);

        return new Champion(null, blurb, null, id, image, info, key, null, name,
                partype, null, null, null, null, null, null, title);
    }

    public void setVersionSuppressed(String dataType, String version) {
        //language=MySQL
        try {
            setVersion(dataType, version);
        } catch (SQLException ignored) {
        }
    }

    public void setVersion(String dataType, String version) throws SQLException {
        //language=MySQL
        final String insertSql = "REPLACE INTO versions(dataType, version) VALUES (?, ?);";
        try (PreparedStatement ps = connection.prepareStatement(insertSql)) {
            ps.setString(1, dataType);
            ps.setString(2, version);
            ps.executeUpdate();
        }
    }

    public String getVersion(String dataType) throws SQLException {
        //language=MySQL
        final String sql = "SELECT version FROM versions WHERE dataType = ?;";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, dataType);
            try (ResultSet resultSet = ps.executeQuery()) {
                return resultSet.next() ? resultSet.getString("version") : null;
            }
        }
    }

    public boolean isVerionUpdatedAfter(String dataType, LocalDateTime dateTime) throws SQLException {
        //language=MySQL
        final String sql = "SELECT version FROM versions WHERE dataType = ? AND last_update > ?;";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, dataType);
            ps.setTimestamp(2, Timestamp.valueOf(dateTime));
            try (ResultSet resultSet = ps.executeQuery()) {
                return resultSet.next();
            }
        }
    }
}

package com.spiderbiggen.leaguestats.database;

import com.spiderbiggen.championgg.model.Build;
import com.spiderbiggen.championgg.model.BuildChoice;
import com.spiderbiggen.championgg.model.BuildOrders;
import com.spiderbiggen.championgg.model.Role;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Collection;
import java.util.TimeZone;

public class ChampionGgDb extends MysqlConnection {

    protected ChampionGgDb() throws SQLException {
        super();
    }

    public static ChampionGgDb create() throws SQLException {
        return new ChampionGgDb();
    }

    @Override
    public void initDatabase() throws SQLException {
        createBuildsTable();
    }

    private void createBuildsTable() throws SQLException {
        //language=MySQL
        final String sql = "CREATE TABLE IF NOT EXISTS builds (" +
                "id INTEGER," +
                "role VARCHAR(11)," +
                "patch VARCHAR(10)," +
                "final_items_hash TEXT," +
                "skill_order_hash TEXT," +
                "summoners_hash TEXT," +
                "first_items_hash TEXT," +
                "last_update TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP," +
                "PRIMARY KEY (id, role)" +
                ");";
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        }
    }

    public void clearOldBuilds() throws SQLException {
        clearOldBuilds(LocalDateTime.now().minus(3, ChronoUnit.DAYS));
    }

    private void clearOldBuilds(LocalDateTime maxAge) throws SQLException {
        //language=MySQL
        final String sql = "DELETE FROM builds WHERE last_update <= ?";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setTimestamp(1, Timestamp.valueOf(maxAge), Calendar.getInstance(TimeZone.getTimeZone("UTC")));
            ps.executeUpdate(sql);
        }
    }

    public void insertBuilds(Collection<Build> builds) throws SQLException {
        //language=MySQL
        final String sql = "INSERT INTO builds (`id`, `role`, `patch`, `final_items_hash`, `skill_order_hash`, `summoners_hash`, `first_items_hash`)\n" +
                "VALUES (?, ?, ?, ?, ?, ?, ?)\n" +
                "ON DUPLICATE KEY UPDATE\n" +
                "  patch = ?, final_items_hash = ?, skill_order_hash = ?, summoners_hash = ?, first_items_hash = ?;";

        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            for (Build build : builds) {
                ps.setInt(1, build.getChampionId());
                ps.setString(2, build.getRole().name());
                ps.setString(3, build.getPatch());
                ps.setString(8, build.getPatch());
                BuildOrders buildOrders = build.getHashes();
                if (buildOrders == null || buildOrders.getFinalitemshashfixed() == null ||
                        buildOrders.getSkillorderhash() == null || buildOrders.getSummonershash() == null ||
                        buildOrders.getFirstitemshash() == null) {
                    continue;
                }
                ps.setString(4, buildOrders.getFinalitemshashfixed().getHighestCount().getHash());
                ps.setString(5, buildOrders.getSkillorderhash().getHighestCount().getHash());
                ps.setString(6, buildOrders.getSummonershash().getHighestCount().getHash());
                ps.setString(7, buildOrders.getFirstitemshash().getHighestCount().getHash());
                ps.setString(9, buildOrders.getFinalitemshashfixed().getHighestCount().getHash());
                ps.setString(10, buildOrders.getSkillorderhash().getHighestCount().getHash());
                ps.setString(11, buildOrders.getSummonershash().getHighestCount().getHash());
                ps.setString(12, buildOrders.getFirstitemshash().getHighestCount().getHash());
                ps.addBatch();
            }
            ps.executeBatch();
        }
    }

    public Build getBuild(int championId, Role role) throws SQLException {
        //language=MySQL
        final String sql = "SELECT `id`, `role`, `patch`, `final_items_hash`, `skill_order_hash`, `summoners_hash`,`first_items_hash` FROM builds WHERE id = ? AND role = ?;";

        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, championId);
            ps.setString(2, role.name());
            try (ResultSet resultSet = ps.executeQuery()) {
                return getBuildFromResultSet(resultSet);
            }
        }
    }

    private Build getBuildFromResultSet(ResultSet resultSet) throws SQLException {
        if (resultSet.next()) {
            Build build = new Build();
            BuildOrders buildOrders = new BuildOrders();
            buildOrders.setFinalitemshashfixed(new BuildChoice());
            buildOrders.setFirstitemshash(new BuildChoice());
            buildOrders.setSummonershash(new BuildChoice());
            buildOrders.setSkillorderhash(new BuildChoice());
            build.setHashes(buildOrders);

            build.setChampionId(resultSet.getInt("id"));
            build.setRole(Role.valueOf(resultSet.getString("role")));
            build.setPatch(resultSet.getString("patch"));

            buildOrders.getFirstitemshash().setHighestCount(new BuildChoice.Hash(resultSet.getString("first_items_hash")));
            buildOrders.getFinalitemshashfixed().setHighestCount(new BuildChoice.Hash(resultSet.getString("final_items_hash")));
            buildOrders.getSummonershash().setHighestCount(new BuildChoice.Hash(resultSet.getString("summoners_hash")));
            buildOrders.getSkillorderhash().setHighestCount(new BuildChoice.Hash(resultSet.getString("skill_order_hash")));
            return build;
        } else {
            return null;
        }
    }

}

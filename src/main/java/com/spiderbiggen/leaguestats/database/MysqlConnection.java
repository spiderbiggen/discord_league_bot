package com.spiderbiggen.leaguestats.database;


import com.mingweisamuel.zyra.enums.Region;
import com.mingweisamuel.zyra.summoner.Summoner;
import net.dv8tion.jda.core.entities.User;
import org.apache.commons.dbcp2.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.Closeable;
import java.sql.*;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map;
import java.util.Optional;

public abstract class MysqlConnection implements Closeable {

    private static final Logger LOGGER = LoggerFactory.getLogger(MysqlConnection.class);
    private static final String ROOT_URL = "jdbc:mysql://"+ System.getenv("RDS_HOSTNAME") + ":" + System.getenv("RDS_PORT");
    private static final String PATH = ROOT_URL + "/spdrbggn_league_discord";
    private static final String USERNAME = System.getenv("RDS_USERNAME");
    private static final String PASSWORD = System.getenv("RDS_PASSWORD");
    private static final String PORT = System.getenv("RDS_PORT");
    private static DataSource pool;

    protected Connection connection;

    protected MysqlConnection() throws SQLException {
        connection = createConnection();
        initDatabase();
    }

    /**
     * Connect to a sample database
     */
    private static Connection createConnection() throws SQLException {
        if (pool == null) {
            BasicDataSource basicDataSource = new BasicDataSource();
            basicDataSource.setDriverClassName("com.mysql.jdbc.Driver");
            basicDataSource.setUrl(PATH);
            basicDataSource.setUsername(USERNAME);
            basicDataSource.setPassword(PASSWORD);
            pool = basicDataSource;
        }
        return pool.getConnection();
    }

//    public static void createDatabase() throws SQLException {
//        //language=MySQL
//        final String sql = "CREATE SCHEMA IF NOT EXISTS spdrbggn_league_discord";
//        try (Connection connection = DriverManager.getConnection(ROOT_URL, "root", "new_password");
//             Statement statement = connection.createStatement()) {
//            statement.executeUpdate(sql);
//        }
//    }

    public abstract void initDatabase() throws SQLException;

    private void createUserTable() throws SQLException {
        //language=MySQL
        final String sql = "CREATE TABLE IF NOT EXISTS users (discord_id BIGINT PRIMARY KEY, summoner_id BIGINT NOT NULL, region VARCHAR(10));";
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        }

    }

    public Optional<Map.Entry<Long, Region>> getSummonerForUser(User user) throws SQLException {
        SimpleEntry<Long, Region> entry = null;
        //language=MySQL
        final String insertSql = "SELECT summoner_id, region FROM users WHERE discord_id = ?;";
        try (PreparedStatement ps = connection.prepareStatement(insertSql)) {
            ps.setLong(1, user.getIdLong());
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    long summonerId = resultSet.getLong("summoner_id");
                    Region region = Region.valueOf(resultSet.getString("region"));
                    entry = new SimpleEntry<>(summonerId, region);
                }
            }
        }
        return Optional.ofNullable(entry);
    }

    public void linkAccounts(User user, Summoner summoner, Region region) throws SQLException {
        //language=MySQL
        final String insertSql = "REPLACE INTO users(discord_id, summoner_id, region) VALUES (?, ?, ?);";
        try (PreparedStatement ps = connection.prepareStatement(insertSql)) {
            ps.setLong(1, user.getIdLong());
            ps.setLong(2, summoner.id);
            ps.setString(3, region.name());
            ps.executeUpdate();
        }
    }

    @Override
    public void close() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

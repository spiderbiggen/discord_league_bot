package com.spiderbiggen.leaguestats.model;

import java.util.Arrays;
import java.util.Optional;

public enum SummonerSpell {

    SUMMONER_BARRIER("SummonerBarrier", "Barrier", 21),
    SUMMONER_BOOST("SummonerBoost", "Cleanse", 1),
    SUMMONER_DARK_STAR_CHAMP_SELECT1("SummonerDarkStarChampSelect1", "Disabled Summoner Spells", 35),
    SUMMONER_DARK_STAR_CHAMP_SELECT2("SummonerDarkStarChampSelect2", "Disabled Summoner Spells", 36),
    SUMMONER_DOT("SummonerDot", "Ignite", 14),
    SUMMONER_EXHAUST("SummonerExhaust", "Exhaust", 3),
    SUMMONER_FLASH("SummonerFlash", "Flash", 4),
    SUMMONER_HASTE("SummonerHaste", "Ghost", 6),
    SUMMONER_HEAL("SummonerHeal", "Heal", 7),
    SUMMONER_MANA("SummonerMana", "Clarity", 13),
    SUMMONER_PORO_RECALL("SummonerPoroRecall", "To the King!", 30),
    SUMMONER_PORO_THROW("SummonerPoroThrow", "Poro Toss", 31),
    SUMMONER_SIEGE_CHAMP_SELECT1("SummonerSiegeChampSelect1", "Nexus Siege: Siege Weapon Slot", 33),
    SUMMONER_SIEGE_CHAMP_SELECT2("SummonerSiegeChampSelect2", "Nexus Siege: Siege Weapon Slot", 34),
    SUMMONER_SMITE("SummonerSmite", "Smite", 11),
    SUMMONER_SNOW_URF_SNOWBALL_MARK("SummonerSnowURFSnowball_Mark", "SummonerSnowURFSnowball_Mark", 39),
    SUMMONER_SNOWBALL("SummonerSnowball", "Mark", 32),
    SUMMONER_TELEPORT("SummonerTeleport", "Teleport", 12);

    private final String id;
    private final String name;
    private final int key;

    SummonerSpell(String id, String name, int key) {
        this.id = id;
        this.name = name;
        this.key = key;
    }

    public static SummonerSpell getFromKey(int key) {
        Optional<SummonerSpell> spell = Arrays.stream(SummonerSpell.values()).filter(s -> s.getKey() == key).findAny();
        return spell.orElseThrow(() -> new IllegalArgumentException("There is no summoner spell with key: " + key));
    }

    /**
     * Gets id
     *
     * @return value of id
     */
    public String getId() {
        return id;
    }

    /**
     * Gets name
     *
     * @return value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets key
     *
     * @return value of key
     */
    public int getKey() {
        return key;
    }
}

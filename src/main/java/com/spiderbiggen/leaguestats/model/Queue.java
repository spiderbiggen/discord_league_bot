package com.spiderbiggen.leaguestats.model;

import java.util.Map;

public final class Queue {

    private static final Map<Long, Queue> queueMap;

    //TODO Update after patch 8.3
    static {
        final String SR = "Summoner's Rift";
        final String TT = "Twisted Treeline";
        final String CS = "Crystal Scar";
        final String HA = "Howling Abyss";
        final String BB = "Butcher's Bridge";
        final String CR = "Cosmic Ruins";
        final String VCP = "Valoran City Park";
        final String OC = "Overcharge";

        Queue CUSTOM_GAME = new Queue("Custom games", "Custom Game");
        Queue BLIND_PICK_5V5 = new Queue(SR, "5v5 Blind Pick");
        Queue RANKED_SOLO_5v5 = new Queue(SR, "5v5 Ranked Solo");
        Queue RANKED_PREMADE_5V5 = new Queue(SR, "5v5 Ranked Premade");
        Queue COOP_VS_AI = new Queue(SR, "Co-op vs AI");
        Queue NORMAL_3V3 = new Queue(TT, "3v3 Normal");
        Queue RANKED_FLEX_3V3 = new Queue(TT, "3v3 Ranked Flex");
        Queue DRAFT_PICK_5V5 = new Queue(SR, "5v5 Draft Pick");
        Queue DOMINION_BLIND_PICK = new Queue(CS, "5v5 Dominion Blind Pick");
        Queue DOMINION_DRAFT_PICK = new Queue(CS, "5v5 Dominion Draft Pick");
        Queue DOMINION_COOP_VS_AI = new Queue(CS, "Dominion Co-op vs AI");
        Queue COOP_VS_AI_INTRO = new Queue(SR, "Co-op vs AI Intro Bot");
        Queue COOP_VS_AI_BEGINNER = new Queue(SR, "Co-op vs AI Beginner Bot");
        Queue COOP_VS_AI_INTERMEDIATE = new Queue(SR, "Co-op vs AI Intermediate Bot");
        Queue RANKED_TEAM_3V3 = new Queue(TT, "3v3 Ranked Team");
        Queue RANKED_TEAM_5V5 = new Queue(SR, "5v5 Ranked Team");
        Queue COOP_VS_AI_3V3 = new Queue(TT, "Co-op vs AI");
        Queue TEAM_BUILDER_5V5 = new Queue(SR, "5v5 Team Builder");
        Queue ARAM_5V5 = new Queue(HA, "5v5 ARAM");
        Queue ONE_FOR_ALL = new Queue(SR, "One for All");
        Queue SNOWDOWN_SHOWDOWN_1V1 = new Queue(HA, "1v1 Snowdown Showdown");
        Queue SNOWDOWN_SHOWDOWN_2V2 = new Queue(HA, "2v2 Snowdown Showdown");
        Queue ULTRA_RAPID_FIRE = new Queue(SR, "Ultra Rapid Fire");
        Queue ONE_FOR_ALL_MIRRORED = new Queue(SR, "Mirrored One for All");
        Queue ULTRA_RAPID_FIRE_COOP_VS_AI = new Queue(SR, "Co-op vs AI Ultra Rapid Fire");
        Queue DOOM_BOTS_R1 = new Queue(SR, "Doom Bots Rank 1");
        Queue DOOM_BOTS_R2 = new Queue(SR, "Doom Bots Rank 2");
        Queue DOOM_BOTS_R3 = new Queue(SR, "Doom Bots Rank 5");
        Queue ASCENSION = new Queue(CS, "Ascension");
        Queue HEXAKILL_6V6 = new Queue(TT, "6v6 Hexakill");
        Queue ARAM_5V5_BB = new Queue(BB, "5v5 ARAM");
        Queue KING_PORO = new Queue(HA, "King Poro");
        Queue NEMESIS = new Queue(SR, "Nemesis");
        Queue BLACK_MARKET_BRAWLERS = new Queue(SR, "Black Market Brawlers");
        Queue NEXUS_SIEGE = new Queue(SR, "Nexus Siege");
        Queue DEFINITELY_NOT_DOMINION = new Queue(CS, "Definitely Not Dominion");
        Queue ULTRA_RAPID_FIRE_ALL_RANDOM = new Queue(SR, "All Random URF");
        Queue ALL_RANDOM = new Queue(SR, "All Random");
        Queue RANKED_DYNAMIC_5V5 = new Queue(SR, "5v5 Ranked Dynamic");
        Queue RANKED_SOLO_5V5 = new Queue(SR, "5v5 Ranked Solo");
        Queue RANKED_FLEX_5V5 = new Queue(SR, "5v5 Ranked Flex");
        Queue BLIND_PICK_3V3 = new Queue(TT, "3v3 Blind Pick");
        Queue BLOOD_HUNT_ASSASSIN = new Queue(SR, "Blood Hunt Assassin");
        Queue DARK_STAR = new Queue(CR, "Dark Star");
        Queue COOP_VS_AI_INTERMEDIATE_3V3 = new Queue(TT, "Co-op vs. AI Intermediate Bot");
        Queue COOP_VS_AI_INTRO_3V3 = new Queue(TT, "Co-op vs. AI Intro Bot");
        Queue COOP_VS_AI_BEGINNER_3V3 = new Queue(TT, "Co-op vs. AI Beginner Bot");
        Queue DOOM_BOTS_DIFFICULTY_VOTING = new Queue(SR, "Doom Bots /w difficulty voting");
        Queue DOOM_BOTS = new Queue(SR, "Doom Bots");
        Queue STAR_GUARDIAN_INVASION_NORMAL = new Queue(VCP, "Star Guardian Invasion: Normal");
        Queue STAR_GUARDIAN_INVASION_ONSLAUGHT = new Queue(VCP, "Star Guardian Invasion: Onslaught");
        Queue AR_URF = new Queue(SR, "ARURF");
        Queue AR_URF_SNOW = new Queue(SR, "PROJECT: Hunters");
        Queue PROJECT_HUNTERS = new Queue(OC, "Snow ARURF");

        //noinspection RedundantTypeArguments (explicit type arguments speedup compilation and analysis time)
        queueMap = Map.<Long, Queue>ofEntries(
                Map.entry(0L, CUSTOM_GAME),
                Map.entry(2L, BLIND_PICK_5V5),
                Map.entry(4L, RANKED_SOLO_5v5),
                Map.entry(6L, RANKED_PREMADE_5V5),
                Map.entry(7L, COOP_VS_AI),
                Map.entry(8L, NORMAL_3V3),
                Map.entry(9L, RANKED_FLEX_3V3),
                Map.entry(14L, DRAFT_PICK_5V5),
                Map.entry(16L, DOMINION_BLIND_PICK),
                Map.entry(17L, DOMINION_DRAFT_PICK),
                Map.entry(25L, DOMINION_COOP_VS_AI),
                Map.entry(31L, COOP_VS_AI_INTRO),
                Map.entry(32L, COOP_VS_AI_BEGINNER),
                Map.entry(33L, COOP_VS_AI_INTERMEDIATE),
                Map.entry(41L, RANKED_TEAM_3V3),
                Map.entry(42L, RANKED_TEAM_5V5),
                Map.entry(52L, COOP_VS_AI_3V3),
                Map.entry(61L, TEAM_BUILDER_5V5),
                Map.entry(65L, ARAM_5V5),
                Map.entry(70L, ONE_FOR_ALL),
                Map.entry(72L, SNOWDOWN_SHOWDOWN_1V1),
                Map.entry(73L, SNOWDOWN_SHOWDOWN_2V2),
                Map.entry(75L, HEXAKILL_6V6),
                Map.entry(76L, ULTRA_RAPID_FIRE),
                Map.entry(78L, ONE_FOR_ALL_MIRRORED),
                Map.entry(83L, ULTRA_RAPID_FIRE_COOP_VS_AI),
                Map.entry(91L, DOOM_BOTS_R1),
                Map.entry(92L, DOOM_BOTS_R2),
                Map.entry(93L, DOOM_BOTS_R3),
                Map.entry(96L, ASCENSION),
                Map.entry(98L, HEXAKILL_6V6),
                Map.entry(100L, ARAM_5V5_BB),
                Map.entry(300L, KING_PORO),
                Map.entry(310L, NEMESIS),
                Map.entry(313L, BLACK_MARKET_BRAWLERS),
                Map.entry(315L, NEXUS_SIEGE),
                Map.entry(317L, DEFINITELY_NOT_DOMINION),
                Map.entry(318L, ULTRA_RAPID_FIRE_ALL_RANDOM),
                Map.entry(325L, ALL_RANDOM),
                Map.entry(400L, DRAFT_PICK_5V5),
                Map.entry(410L, RANKED_DYNAMIC_5V5),
                Map.entry(420L, RANKED_SOLO_5V5),
                Map.entry(430L, BLIND_PICK_5V5),
                Map.entry(440L, RANKED_FLEX_5V5),
                Map.entry(450L, ARAM_5V5),
                Map.entry(460L, BLIND_PICK_3V3),
                Map.entry(470L, RANKED_FLEX_3V3),
                Map.entry(600L, BLOOD_HUNT_ASSASSIN),
                Map.entry(610L, DARK_STAR),
                Map.entry(800L, COOP_VS_AI_INTERMEDIATE_3V3),
                Map.entry(810L, COOP_VS_AI_INTRO_3V3),
                Map.entry(820L, COOP_VS_AI_BEGINNER_3V3),
                Map.entry(830L, COOP_VS_AI_INTRO),
                Map.entry(840L, COOP_VS_AI_BEGINNER),
                Map.entry(850L, COOP_VS_AI_INTERMEDIATE),
                Map.entry(900L, AR_URF),
                Map.entry(910L, ASCENSION),
                Map.entry(920L, KING_PORO),
                Map.entry(940L, NEXUS_SIEGE),
                Map.entry(950L, DOOM_BOTS_DIFFICULTY_VOTING),
                Map.entry(960L, DOOM_BOTS),
                Map.entry(980L, STAR_GUARDIAN_INVASION_NORMAL),
                Map.entry(990L, STAR_GUARDIAN_INVASION_ONSLAUGHT),
                Map.entry(1000L, PROJECT_HUNTERS),
                Map.entry(1010L, AR_URF_SNOW)
        );
    }

    public final String map;
    public final String description;

    private Queue(String map, String description) {
        this.map = map;
        this.description = description;
    }

    public static Queue getQueueById(long id) {
        if(!queueMap.containsKey(id)) return null;
        return queueMap.get(id);
    }

}

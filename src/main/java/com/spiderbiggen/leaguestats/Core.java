package com.spiderbiggen.leaguestats;

import com.mingweisamuel.zyra.RiotApi;
import com.mingweisamuel.zyra.enums.Region;
import com.mingweisamuel.zyra.lolStaticData.ChampionList;
import com.mingweisamuel.zyra.lolStaticData.Realm;
import com.spiderbiggen.championgg.ChampionGG;
import com.spiderbiggen.championgg.model.Build;
import com.spiderbiggen.leaguestats.commands.*;
import com.spiderbiggen.leaguestats.database.ChampionGgDb;
import com.spiderbiggen.leaguestats.database.RiotDb;
import com.spiderbiggen.leaguestats.event.handlers.MessageListener;
import com.spiderbiggen.leaguestats.registries.CommandRegistry;
import com.spiderbiggen.leaguestats.util.LoggerUtils;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.exceptions.RateLimitedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.login.LoginException;
import java.io.Closeable;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Core implements Closeable {

    private static final Logger LOGGER = LoggerFactory.getLogger(Core.class);
    private JDA jda;
    private RiotApi api;
    private ScheduledExecutorService executorService;

    public Core() {
        String key = System.getenv("LD_DISCORD");
        api = RiotApi.newInstance(System.getenv("LD_RIOT"));
        executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(this::updateDatabase, 0, 3, TimeUnit.HOURS);
        registerCommands();
        connectBot(key, api);
        Runtime.getRuntime().addShutdownHook(new Thread(this::close));
    }

    public static void main(String... args) {
        new Core();
    }

    private void updateDatabase() {
        updateRealm();
        updateChampions();
        updateBuilds();
    }

    private void updateBuilds() {
        try (ChampionGgDb connection = ChampionGgDb.create()) {
            List<Build> roles = new ChampionGG(System.getenv("LD_CHAMPION_GG")).getRolesAndBuilds();
            connection.insertBuilds(roles);
            LOGGER.info("Builds table is up-to-date.");
        } catch (SQLException e) {
            LoggerUtils.logGenericDatabaseError(LOGGER, e);
        }
    }

    private void updateChampions() {
        CompletableFuture<ChampionList> future = api.staticData.getChampionListAsync(Region.EUW, Arrays.asList("image", "info", "partype", "blurb", "skins"));
        future.thenAcceptAsync(championList -> {
            try (RiotDb connection = RiotDb.create()) {
                connection.insertChampionsMulti(championList);
                LOGGER.info("Champions & Skins tables are up-to-date.");
            } catch (SQLException e) {
                LoggerUtils.logGenericDatabaseError(LOGGER, e);
            }
        });
    }

    private void updateRealm() {
        CompletableFuture<Realm> realmFuture = api.staticData.getRealmAsync(Region.EUW);
        realmFuture.thenAcceptAsync(realm -> {
            if (realm == null) return;
            try (RiotDb connection = RiotDb.create()) {
                realm.n.forEach(connection::setVersionSuppressed);
                LOGGER.info("Versions table is up-to-date.");
            } catch (SQLException e) {
                LoggerUtils.logGenericDatabaseError(LOGGER, e);
            }
        });
    }

    private void connectBot(String key, RiotApi api) {
        LOGGER.info("Connecting to discord servers");
        try {
            jda = new JDABuilder(AccountType.BOT).setToken(key)
                    .setAutoReconnect(true)
                    .addEventListener(new MessageListener(api))
                    .buildAsync();
        } catch (LoginException | RateLimitedException e) {
            LOGGER.error(e.getMessage());
        }
    }

    private void registerCommands() {
        CommandRegistry.register(new Help());
        CommandRegistry.register(new Random());
        CommandRegistry.register(new Spectate());
        CommandRegistry.register(new RegisterSummoner());
        CommandRegistry.register(new SpectateMe());
        CommandRegistry.register(new ChampionRoleBuild());
    }

    @Override
    public void close() {
        LOGGER.info("Shutting down...");
        executorService.shutdown();
        try {
            api.close();
        } catch (IOException e) {
            LoggerUtils.logGenericRiotApiClosingError(LOGGER, e);
        }
        jda.shutdown();
    }
}

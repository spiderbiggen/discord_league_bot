package com.spiderbiggen.leaguestats.registries;

import com.mingweisamuel.zyra.RiotApi;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.spiderbiggen.leaguestats.commands.Command;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class CommandRegistry {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommandRegistry.class);
    private static final Map<String, Command> commandMap = new ConcurrentHashMap<>();

    public static void register(Command command) {
        commandMap.put(command.getName(), command);
        LOGGER.info("Registered command: {}", command.getName());
    }

    public static boolean Unregister(String command) {
        LOGGER.info("Unregistered command: {}", command);
        return commandMap.remove(command) != null;
    }

    public static void execute(String content, User user, MessageChannel messageChannel, RiotApi api) {
        String[] args = content.split("\\s");
        String command = args[0].toLowerCase();
        args = shiftArray(args);
        if (commandMap.containsKey(command)) {
            Command command1 = commandMap.get(command);
            command1.execute(args, user, messageChannel, api);
            LOGGER.debug("Executed: {}", command1.getName());
        }
    }

    private static String[] shiftArray(String[] array) {
        String[] args = new String[array.length - 1];
        System.arraycopy(array, 1, args, 0, array.length - 1);
        return args;
    }

    public static List<Command> getCommands() {
        return commandMap.values().stream().sorted().collect(Collectors.toList());
    }

}

package com.spiderbiggen.leaguestats.commands;

import com.mingweisamuel.zyra.RiotApi;
import com.spiderbiggen.championgg.model.Role;
import com.spiderbiggen.leaguestats.util.MessageUtils;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

public abstract class Command implements Comparable<Command> {

    private static final Logger LOGGER = LoggerFactory.getLogger(Command.class);
    private String name;

    public Command(@Nonnull String name) {
        this.name = name;
    }

    public String getLongNameFromArgs(String[] messageParts, int startIndex) {
        StringBuilder sb = new StringBuilder();
        for (int i = startIndex; i < messageParts.length; i++) {
            sb.append(messageParts[i]).append(' ');
        }
        return sb.toString().trim();
    }

    public final String getName() {
        return name.toLowerCase();
    }

    public abstract void execute(String[] args, User user, MessageChannel channel, RiotApi api);

    public abstract String getHelp();

    @Override
    public int compareTo(@Nonnull Command o) {
        return this.getName().compareTo(o.getName());
    }

    public Role getRoleFromArg(String arg, MessageChannel channel) {
        Role role = null;
        try {
            role = Role.parse(arg);
        } catch (IllegalArgumentException e) {
            LOGGER.error(e.getMessage());
            MessageUtils.sendMessage(channel, "This is not a valid Role: " + arg);
        }
        return role;
    }
}

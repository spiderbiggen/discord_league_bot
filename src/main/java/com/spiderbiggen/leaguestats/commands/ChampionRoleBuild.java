package com.spiderbiggen.leaguestats.commands;

import com.mingweisamuel.zyra.RiotApi;
import com.mingweisamuel.zyra.lolStaticData.Champion;
import com.spiderbiggen.championgg.model.Build;
import com.spiderbiggen.championgg.model.Role;
import com.spiderbiggen.leaguestats.database.ChampionGgDb;
import com.spiderbiggen.leaguestats.database.RiotDb;
import com.spiderbiggen.leaguestats.util.HelpFactory;
import com.spiderbiggen.leaguestats.util.ImageHelper;
import com.spiderbiggen.leaguestats.util.LoggerUtils;
import com.spiderbiggen.leaguestats.util.MessageUtils;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.sql.SQLException;

public class ChampionRoleBuild extends Command {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChampionRoleBuild.class);

    public ChampionRoleBuild() {
        super("build");
    }

    @Override
    public void execute(String[] args, User user, MessageChannel channel, RiotApi api) {
        if (channel == null) return;
        if (args.length < 2) {
            MessageUtils.sendMessage(channel, getHelp());
            return;
        }
        Role role = getRoleFromArg(args[0], channel);
        if (role == null) return;

        Champion champion = getChampion(getLongNameFromArgs(args, 1), channel);
        if (champion == null) return;

        Build build = getBuild(role, champion, channel);
        if (build == null) return;

        BufferedImage image = getBufferedImage(build);
        if (image == null) return;
        createEmbed(champion, role, image, channel);
    }

    private BufferedImage getBufferedImage(Build build) {
        BufferedImage image = null;
        try {
            image = ImageHelper.createBuildImage(build);
        } catch (IOException e) {
            LOGGER.error("Something went wrong reading/writing/downloading images.", e);
        }
        return image;
    }

    private Build getBuild(Role role, Champion champion, MessageChannel channel) {
        Build build = null;
        try (ChampionGgDb connection = ChampionGgDb.create()) {
            build = connection.getBuild(champion.id, role);
        } catch (SQLException e) {
            LoggerUtils.logGenericDatabaseError(LOGGER, e);
        }
        if (build == null) {
            MessageUtils.sendMessage(channel, "No build available for this combination.");
        }
        return build;
    }

    private Champion getChampion(String championName, MessageChannel channel) {
        Champion champion = null;
        try (RiotDb connection = RiotDb.create()) {
            champion = connection.getChampionByName(championName);
        } catch (SQLException e) {
            LoggerUtils.logGenericDatabaseError(LOGGER, e);
        }
        if (champion == null) {
            MessageUtils.sendMessage(channel, "Champion not found.");
        }
        return champion;
    }

    private void createEmbed(Champion champion, Role role, BufferedImage image, MessageChannel channel) {
        String fileName = String.format("%s_%s.png", champion.name, role.toString());
        byte[] data = ImageHelper.toByteArray(image);
        MessageUtils.sendFile(channel, fileName, data);
    }

    @Override
    public String getHelp() {
        return new HelpFactory().startCommand(getName())
                .addRequiredParameter("role")
                .addRequiredParameter("champion")
                .endCommand()
                .build();
    }


}

package com.spiderbiggen.leaguestats.commands;

import com.mingweisamuel.zyra.RiotApi;
import com.mingweisamuel.zyra.enums.Region;
import com.mingweisamuel.zyra.summoner.Summoner;
import com.spiderbiggen.leaguestats.database.RiotDb;
import com.spiderbiggen.leaguestats.util.HelpFactory;
import com.spiderbiggen.leaguestats.util.LoggerUtils;
import com.spiderbiggen.leaguestats.util.MessageUtils;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class SpectateMe extends Spectate {

    private static final Logger LOGGER = LoggerFactory.getLogger(SpectateMe.class);

    public SpectateMe() {
        super("spectateme");
    }

    @Override
    public void execute(String[] args, User user, MessageChannel channel, RiotApi api) {
        if (channel == null) return;
        Map.Entry<Long, Region> pair = null;
        try (RiotDb connection = RiotDb.create()) {
            pair = connection.getSummonerForUser(user).orElse(null);
        } catch (SQLException e) {
            LoggerUtils.logGenericDatabaseError(LOGGER, e);
        }
        if (pair != null) {
            Region region = pair.getValue();
            Long summonerId = pair.getKey();
            CompletableFuture<Summoner> future = api.summoners.getBySummonerIdAsync(region, summonerId);
            future.thenAcceptAsync(summoner -> sendSpectate(region, summoner, channel, api));
        } else {
            MessageUtils.sendMessage(channel, String.format("No summoner has been linked to the user: %s%n", user.getAsMention()));
        }
    }

    @Override
    public String getHelp() {
        return new HelpFactory().startCommand(getName()).endCommand().build();
    }
}

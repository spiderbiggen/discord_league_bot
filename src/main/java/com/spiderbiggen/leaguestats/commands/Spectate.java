package com.spiderbiggen.leaguestats.commands;

import com.mingweisamuel.zyra.RiotApi;
import com.mingweisamuel.zyra.enums.Region;
import com.mingweisamuel.zyra.lolStaticData.Champion;
import com.mingweisamuel.zyra.spectator.CurrentGameInfo;
import com.mingweisamuel.zyra.spectator.CurrentGameParticipant;
import com.mingweisamuel.zyra.summoner.Summoner;
import com.spiderbiggen.leaguestats.database.RiotDb;
import com.spiderbiggen.leaguestats.model.Queue;
import com.spiderbiggen.leaguestats.util.HelpFactory;
import com.spiderbiggen.leaguestats.util.LoggerUtils;
import com.spiderbiggen.leaguestats.util.MessageUtils;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.entities.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.time.Instant;
import java.util.concurrent.CompletableFuture;

import static java.lang.String.format;

public class Spectate extends Command {

    private static final Logger LOGGER = LoggerFactory.getLogger(Spectate.class);
    private static final String TEAM_0_NAME = "Blue Team";
    private static final String TEAM_1_NAME = "Red Team";
    private static final String MATCH_HISTORY_TEXT_PATTERN = "%s playing %s on %s%nCheck [Match history](%s) after the game is finished";
    private static final String MATCH_HISTORY_URL_PATTERN = "http://matchhistory.euw.leagueoflegends.com/en/#match-details/%s/%d/%d";

    public Spectate() {
        super("spectate");
    }

    public Spectate(String name) {
        super(name);
    }

    @Override
    public void execute(String[] args, User user, MessageChannel channel, RiotApi api) {
        if (channel == null) return;
        if (args.length >= 2) {
            Region region = Region.parse(args[0]);
            String summonerName = getLongNameFromArgs(args, 1);
            CompletableFuture<Summoner> future = api.summoners.getBySummonerNameAsync(region, summonerName);
            future.thenAcceptAsync(summoner -> sendSpectate(region, summoner, channel, api));
        } else {
            MessageUtils.sendMessage(channel, getHelp());
        }
    }

    protected final void sendSpectate(Region region, Summoner summoner, MessageChannel channel, RiotApi api) {
        if (summoner == null || region == null) return;
        CurrentGameInfo gameInfo = api.spectator.getCurrentGameInfoBySummoner(region, summoner.id);
        LOGGER.info(String.format("Sending spectator embed for{%s}", summoner.name));
        MessageUtils.sendMessage(channel, createSpectatorEmbed(region, summoner, gameInfo));
    }

    @Override
    public String getHelp() {
        return new HelpFactory().startCommand(getName())
                .addRequiredParameter("region")
                .addRequiredParameter("summoner name")
                .endCommand().build();
    }

    private MessageEmbed createSpectatorEmbed(Region region, Summoner requestedSummoner, CurrentGameInfo gameInfo) {
        Queue queue = Queue.getQueueById(gameInfo.gameQueueConfigId);
        String description = queue != null ? queue.description : "";
        String map = queue != null ? queue.map : "";
        final String match = format(MATCH_HISTORY_URL_PATTERN, region.platform, gameInfo.gameId, requestedSummoner.id);
        final String gameType = format(MATCH_HISTORY_TEXT_PATTERN,
                requestedSummoner.name, description, map, match);

        StringBuilder team0 = new StringBuilder();
        StringBuilder team1 = new StringBuilder();
        String newLine = System.lineSeparator();
        try (RiotDb connection = RiotDb.create()) {
            for (CurrentGameParticipant participant : gameInfo.participants) {
                try {
                    Champion champ = connection.getChampionById(participant.championId);
                    String summoner = format("%-16s\t(%s)", participant.summonerName, champ.name);
                    if (participant.teamId == 100) team0.append(summoner).append(newLine);
                    else team1.append(summoner).append(newLine);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } catch (SQLException e) {
            LoggerUtils.logGenericDatabaseError(LOGGER, e);
        }
        return MessageUtils.createEmbedBuilder()
                .setTitle(format("Spectator data for %s:", requestedSummoner.name))
                .setDescription(gameType)
                .addField(TEAM_0_NAME, MessageUtils.wrapInCodeBlock(team0.toString()), false)
                .addField(TEAM_1_NAME, MessageUtils.wrapInCodeBlock(team1.toString()), false)
                .setTimestamp(Instant.ofEpochMilli(gameInfo.gameStartTime))
                .build();
    }
}

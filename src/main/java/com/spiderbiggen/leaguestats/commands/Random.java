package com.spiderbiggen.leaguestats.commands;

import com.mingweisamuel.zyra.RiotApi;
import com.mingweisamuel.zyra.lolStaticData.Champion;
import com.spiderbiggen.championgg.model.Role;
import com.spiderbiggen.leaguestats.database.RiotDb;
import com.spiderbiggen.leaguestats.util.HelpFactory;
import com.spiderbiggen.leaguestats.util.LoggerUtils;
import com.spiderbiggen.leaguestats.util.MessageUtils;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.entities.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.time.ZonedDateTime;
import java.util.Arrays;

public class Random extends Command {

    private static final Logger LOGGER = LoggerFactory.getLogger(Random.class);

    public Random() {
        super("random");
    }

    @Override
    public void execute(String[] args, User user, MessageChannel channel, RiotApi api) {
        Role role = null;
        if (args.length == 1) {
            try {
                role = Role.parse(args[0].replaceAll("[^a-zA-Z]", ""));
            } catch (IllegalArgumentException e) {
                String sb = getHelp() + "\n**Available role**:\n" + Arrays.toString(Role.values());
                MessageUtils.sendMessage(channel, sb);
                return;
            }
        }
        try (RiotDb connection = RiotDb.create()) {
            Champion champion = connection.getRandomChampion(role);
            String championVersion = connection.getVersion("champion");
            int skinId = connection.getRandomSkin(champion.id).num;
            LOGGER.debug("Sending Champion{%s} embed", champion.name);
            MessageUtils.sendMessage(channel, createChampionEmbed(champion, championVersion, skinId));
        } catch (SQLException e) {
            LoggerUtils.logGenericDatabaseError(LOGGER, e);
        }
    }

    @Override
    public String getHelp() {
        return new HelpFactory().startCommand(getName()).addOptionalParameter("role").endCommand().build();
    }

    private MessageEmbed createChampionEmbed(Champion champion, String championVersion, int skinId) {
        final String authorUrl = String.format("https://universe.leagueoflegends.com/en_GB/champion/%s", champion.key);
        final String authorIconUrl = String.format("http://ddragon.leagueoflegends.com/cdn/%s/img/champion/%s.png", championVersion, champion.key);
        final String imageUrl = String.format("http://ddragon.leagueoflegends.com/cdn/img/champion/splash/%s_%d.jpg", champion.key, skinId);
        String championTitle = champion.title.substring(0, 1).toUpperCase() + champion.title.substring(1);
        final String description = String.format("***%1$s***%n%2$s%nCheck [champion.gg/%3$s](http://champion.gg/champion/%3$s) page for %4$s",
                championTitle, champion.blurb, champion.key, champion.name);
        return MessageUtils.createEmbedBuilder()
                .setImage(imageUrl)
                .setAuthor(champion.name, authorUrl, authorIconUrl)
                .setTimestamp(ZonedDateTime.now())
                .setDescription(description)
                .build();
    }
}

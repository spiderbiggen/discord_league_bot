package com.spiderbiggen.leaguestats.commands;

import com.mingweisamuel.zyra.RiotApi;
import com.mingweisamuel.zyra.enums.Region;
import com.mingweisamuel.zyra.summoner.Summoner;
import com.spiderbiggen.leaguestats.Core;
import com.spiderbiggen.leaguestats.database.RiotDb;
import com.spiderbiggen.leaguestats.util.HelpFactory;
import com.spiderbiggen.leaguestats.util.LoggerUtils;
import com.spiderbiggen.leaguestats.util.MessageUtils;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.SQLException;

import static java.lang.String.format;
import static com.spiderbiggen.leaguestats.util.MessageUtils.sendMessage;

public class RegisterSummoner extends Command {

    private static final Logger LOGGER = LoggerFactory.getLogger(RegisterSummoner.class);

    public RegisterSummoner() {
        super("register");
    }

    @Override
    public void execute(String[] args, User user, MessageChannel channel, RiotApi api) {
        if (args.length < 2) throw new IllegalArgumentException();
        Region region = Region.valueOf(args[0].toUpperCase());
        String summonerName = getLongNameFromArgs(args, 1);
        try (RiotDb connection = RiotDb.create()) {
            Summoner summoner = api.summoners.getBySummonerName(region, summonerName);
            if (summoner == null) {
                MessageUtils.sendMessage(channel, format("The user: %s does not exist in the %s region.", summonerName, region));
                return;
            }
            connection.linkAccounts(user, summoner, region);
            MessageUtils.sendMessage(channel, format("Added %s,%s for %s.", summonerName, region, user.getAsMention()));
        } catch (IllegalArgumentException ex) {
            MessageUtils.sendMessage(channel, getHelp());
        } catch (SQLException e) {
            LoggerUtils.logGenericDatabaseError(LOGGER, e);
        }
    }

    @Override
    public String getHelp() {
        return new HelpFactory().startCommand(getName())
                .addRequiredParameter("region")
                .addRequiredParameter("username")
                .endCommand().build();
    }
}

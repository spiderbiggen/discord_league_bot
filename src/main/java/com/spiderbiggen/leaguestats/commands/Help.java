package com.spiderbiggen.leaguestats.commands;

import com.mingweisamuel.zyra.RiotApi;
import com.mingweisamuel.zyra.enums.Region;
import com.spiderbiggen.leaguestats.event.handlers.MessageListener;
import com.spiderbiggen.championgg.model.Role;
import com.spiderbiggen.leaguestats.registries.CommandRegistry;
import com.spiderbiggen.leaguestats.util.HelpFactory;
import com.spiderbiggen.leaguestats.util.MessageUtils;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

import java.util.Arrays;
import java.util.Collection;

public class Help extends Command {

    public Help() {
        super("help");
    }

    @Override
    public void execute(String[] args, User user, MessageChannel channel, RiotApi api) {
        Collection<Command> commands = CommandRegistry.getCommands();
        EmbedBuilder sb = MessageUtils.createEmbedBuilder();
        sb.setTitle("Available commands");
        sb.appendDescription(String.format("The prefix `%s` can be replaced by a mention to this bot.", MessageListener.PREFIX));
        sb.appendDescription("\n**Available regions**:\n");
        sb.appendDescription(Arrays.toString(Arrays.stream(Region.values()).filter(r -> r.isSupported).toArray()));
        sb.appendDescription("\n**Available roles**:\n");
        sb.appendDescription(Arrays.toString(Role.values()));
        commands.forEach(s -> sb.addField(s.getName(), s.getHelp(), false));
        MessageUtils.sendMessage(channel, sb.build());
    }

    @Override
    public String getHelp() {
        return new HelpFactory().startCommand(getName()).endCommand().build();
    }
}
